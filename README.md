# Ansible and Redfish training
In this repo you can find the code used through the Ansible training and Redfish Premier.  
This repo has two folders:  
 - ansible: inventory file and playbooks written during the training.
 - redfish: postman collection with all HTTP calls used.

/Miguel Sama (miguel.sama@dell.com)
2020
